$(function () {
    $('.popular-slick-slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: "<i class='fa fa-angle-left'></i>",
        nextArrow: "<i class='fa fa-angle-right'></i>",
    });
});

$(document).on('click', ".create-post-button", function () {
    var form = $('.create-post-form');

    $(this).slideUp();
    $(form).slideDown();
});

$(document).on('click', ".cancel-post", function () {
    var button = $('.create-post-button'),
        form = $('.create-post-form');

    $(form).slideUp();
    $(button).slideDown();
});