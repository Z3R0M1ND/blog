{include file='header.tpl'}
<div class="post-wrapper">
    <div class="container">
        <a class="home-button" href="/"><i class="fa fa-home"></i></a>
        <article class="post" data-post-id="{$post.id}">
            <div class="meta">
                <span class="author"><i class="fa fa-user"></i>{$post.author}</span>
            </div>
            <div class="content">{$post.content|nl2br}</div>
            <div class="meta">
                <span class="comments"><i class="fa fa-comments"></i>{$post.comments}</span>
                <span class="date"><i class="fa fa-calendar"></i>{$post.posted|date_format:"d.m.Y в H:i:s"}</span>
            </div>
        </article>
        <div class="comments-wrapper">
            <form class="comments-form" method="POST" action="/create-comment/">
                <input type="hidden" name="id" value="{$post.id}" />
                <input class="col-xs-3" type="text" name="author" required minlength="3" maxlength="16" placeholder="Как вас зовут?" />
                <input class="col-xs-7" type="text" name="message" required minlength="25" maxlength="255" placeholder="Что вы хотите написать?" />
                <button class="col-xs-2" type="submit">Ответить</button>
            </form>
            <div class="comments-list">
                {foreach $comments as $comment}
                <div class="comment" id="comment-{$comment.id}">
                    <div class="author">{$comment.author} <span>{$comment.posted|date_format:"d.m.Y в H:i:s"}</span></div>
                    <div class="comment-text">{$comment.comment}</div>
                </div>
                {foreachelse}
                Комментариев нет
                {/foreach}
            </div>
        </div>
    </div>
</div>
{include file='footer.tpl'}