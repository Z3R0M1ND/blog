<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=yes, maximum-scale=1" />
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <title>{$title}</title>

    <meta name="robots" content="noindex,follow"/>

{if isset($styles) && count($styles) > 0}{foreach $styles as $style}
    <link rel='stylesheet' id='{$style.id}'  href='{$style.path}?ver={$style.version}' type='text/css' media='{$style.media}' />
{/foreach}{/if}
</head>
<body>
<header class="header">
    <div class="container text-center">
        Micro<span>Blog</span>
    </div>
</header>