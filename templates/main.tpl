{include file='header.tpl'}
{if count($popular_posts) > 0}<section class="popular-posts">
    <div class="container">
        <h2>Популярные записи</h2>
        <div class="popular-slick-slider">
            {foreach $popular_posts as $post}
                <article class="col-xs-12 post" data-post-id="{$post.id}">
                    <div class="meta">
                        <span class="author"><i class="fa fa-user"></i>{$post.author}</span>
                    </div>
                    <div class="content">{$post.content|truncate:650}</div>
                    <div class="meta">
                        <span class="comments"><i class="fa fa-comments"></i>{$post.comments}</span>
                        <span class="date"><i class="fa fa-calendar"></i>{$post.posted|date_format:"d.m.Y в H:i:s"}</span>
                    </div>
                    <a class="button" href="/blog/{$post.id}/">Подробнее</a>
                </article>
            {/foreach}
        </div>
    </div>
</section>{/if}
<section class="create-post">
    <div class="container">
        <div class="create-post-button">
            <i class="fa fa-align-left"></i>
            <div class="description">
                Незнаете с кем подилиться своими идеями или же другими мыслями?
                <span>Расскажите об этом всему миру.</span>
            </div>
        </div>
        <form class="create-post-form" method="POST" action="/create-post/">
            <label class="col-xs-12 field textarea">
                <textarea name="content" placeholder="Содержание записи" required minlength="250"></textarea>
            </label>
            <label class="col-xs-8 field input">
                <input type="text" name="author" placeholder="Как вас запомнят люди" required minlength="3" maxlength="16" />
            </label>
            <div class="col-xs-2 text-center cancel-post">Отмена</div>
            <button class="col-xs-2" type="submit">Опубликовать</button>
            <div class="clearfix"></div>
        </form>
    </div>
</section>
{if count($posts) > 0}<section class="main-posts">
    <div class="container">
        <h2>Последние записи</h2>
        {foreach $posts as $post}
            <article class="col-xs-4 post" data-post-id="{$post.id}">
                <div class="meta">
                    <span class="author"><i class="fa fa-user"></i>{$post.author}</span>
                </div>
                <div class="content">{$post.content|truncate:100}</div>
                <div class="meta">
                    <span class="comments"><i class="fa fa-comments"></i>{$post.comments}</span>
                    <span class="date"><i class="fa fa-calendar"></i>{$post.posted|date_format:"d.m.Y в H:i:s"}</span>
                    <div class="clearfix"></div>
                </div>
                <a class="button" href="/blog/{$post.id}/">Подробнее</a>
            </article>
        {/foreach}
    </div>
</section>{/if}
{include file='footer.tpl'}