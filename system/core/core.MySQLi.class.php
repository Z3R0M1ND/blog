<?php
class Core_MySQLi
{
    private static $instance = null;
    public static function getInstance()
    {
        if (self::$instance == NULL) self::$instance = new self();
        return self::$instance;
    }

    public $MySQLi;
    public $query_list = [];

    function __construct()
    {
        $_database_config_path = CONFIGS . 'config.database.xml';
        if (file_exists($_database_config_path)) $_config_database = simplexml_load_file($_database_config_path);
        else exit('Database config file not found at <b>' . $_database_config_path . '</b>');

        $this->MySQLi = new MySQLi($_config_database->host, $_config_database->user, $_config_database->password, $_config_database->database, (int) $_config_database->port);
        if ($this->MySQLi->connect_errno) exit("Не удалось подключиться к MySQL: (" . $this->MySQLi->connect_errno . ") " . $this->MySQLi->connect_error);
        $this->MySQLi->query("SET NAMES `utf8`");
    }

    /**
     * @param $query
     * @return bool|mysqli_result
     */
    public function query($query)
    {
        $result = $this->MySQLi->query($query);

        if ($result) $this->query_list[] = "[MySQL QUERY] :: " . $query;
        return $result;
    }

    /**
     * @param $table
     * @param $data
     * @param $where
     * @param bool $_print
     * @return bool|mysqli_result
     */
    public function update($table, $data, $where, $_print = false)
    {
        $query = "UPDATE `" . $table . "` SET ";

        foreach ($data as $key => $value)
            $query .= "`" . $key . "` = '" . $value . "', ";

        $query = substr($query, 0, strlen($query) - 2) . " WHERE " . $where . ";";

        if ($_print) exit($query);
        $result = $this->query($query);

        if ($result) $this->query_list[] = "[MySQL UPDATE] :: " . $query;
        return $result;
    }

    /**
     * @param $table
     * @param $data
     * @return bool|mysqli_result
     */
    public function insert($table, $data)
    {
        $query = "INSERT INTO `" . $table . "` (";

        foreach ($data as $key => $value)
            $query .= '`' . $key . '`,';

        $query = substr($query, 0, strlen($query) - 1) . ") VALUES(";

        foreach ($data as $key => $value)
            $query .= "'" . str_replace("'", "\'", $value) . "',";

        $query = substr($query, 0, strlen($query) - 1) . ");";

        $result = $this->query($query);

        if ($result) $this->query_list[] = "[MySQL INSERT] :: " . $query;
        return $result;
    }
}