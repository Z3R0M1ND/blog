<?php
interface iCore
{
    public function init();
}

interface iController
{
    public function method_init();
}

class Controller
{
    public $Core;
    public $Smarty;

    public $Model;

    public function __construct()
    {
        $this->Smarty = new Smarty();
        $this->Smarty->setTemplateDir(TEMPLATES);
        $this->Smarty->setCompileDir(TEMPLATES_COMPILE);
        $this->Smarty->setCacheDir(CACHE);

        $this->Core = Core::getInstance();
    }
}

class Core implements iCore
{
    private static $instance = null;
    public static function getInstance()
    {
        if (self::$instance == NULL) self::$instance = new self();
        return self::$instance;
    }

    public $MySQLi;
    public $Smarty;

    function __construct()
    {
        # Подключение MySQLi
        require_once (CORE . 'core.MySQLi.class.php');
        $this->MySQLi = Core_MySQLi::getInstance();

        # Подключение Smarty
        require_once LIBS . 'Smarty/Smarty.class.php';

        $this->Smarty = new Smarty();
        $this->Smarty->setTemplateDir(TEMPLATES);
        $this->Smarty->setCompileDir(TEMPLATES_COMPILE);
        $this->Smarty->setCacheDir(CACHE);
    }

    # Initialization engine core (route)
    public function init()
    {
        $routes = explode('/', $_SERVER['REQUEST_URI']);

        $_controller = "Main";
        $_method = "init";

        if (isset ($_GET['controller']))
        {
            $_controller = ucfirst(strtolower($_GET['controller']));
            if (isset ($_GET['method']) && !empty($_GET['method'])) $_method = ucfirst(strtolower($_GET['method']));
        }
        else
        {
            if (isset ($routes[1]) && !empty($routes[1])) $_controller = ucfirst(strtolower($routes[1]));
            if (isset ($routes[2]) && !empty($routes[2])) $_method = ucfirst(strtolower($routes[2]));
        }

        $controller = 'Controller_' . $_controller;
        $method = 'method_' . $_method;

        $_model_path = MODELS . strtolower($_controller) . '.model.php';
        if (file_exists ($_model_path)) require_once $_model_path;

        $_controller_path = CONTROLLERS . strtolower($_controller) . '.controller.php';
        if (file_exists ($_controller_path)) require_once $_controller_path;
        else Core::ErrorException(404, $_controller_path);

        $controller = new $controller;
        if (method_exists ($controller, $method)) $controller->$method();
        else Core::ErrorException(404, 'method');
    }

    /**
     * @param $_code
     * @param bool $_debug
     *
     * Except 404 error
     */
    static function ErrorException($_code, $_debug = false)
    {
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");

        $Smarty = new Smarty();

        $exception = $Smarty->createTemplate($_code . '.tpl');

        switch ($_code)
        {
            case 404:
                $_title = "Страница не найдена";
                $_description = "Данной страницы не существует";
                break;

            default:
                $_title = "Ошибка " . $_code;
                $_description = "Возникла ошибка";
        }

        if ($_debug) $_title .= " [" . $_debug . "]";

        $exception->assign('code', $_code);
        $exception->assign('title', $_title);
        $exception->assign('description', $_description);

        $exception->display();
        exit;
    }
}