<?php
class Controller_Main extends Controller implements iController
{
    # Initialization method
    public function method_init()
    {
        $this->Model = new Model_Main();

        $template = $this->Core->Smarty->createTemplate('main.tpl');
        $this->_assign($template);
        $template->assign('popular_posts', $this->Model->get_posts(['limit' => 5, 'orderby' => 'popular']));
        $template->assign('posts', $this->Model->get_posts(['limit' => 25]));

        $template->display();
    }

    # Create post method
    public function method_create()
    {
        $this->Model = new Model_Main();

        $_author = htmlspecialchars(strip_tags($_POST['author']));
        $_content = htmlspecialchars(strip_tags($_POST['content']));

        $_post = $this->Model->create_post($_author, $_content);
        if ($_post) header("Location: /blog/" . $_post . "/");
        else die("MySQL Error: (" . $this->Model->Core->MySQLi->MySQLi->errno . ") " . $this->Model->Core->MySQLi->MySQLi->error);
    }

    # Create comment method
    public function method_comment()
    {
        $this->Model = new Model_Main();

        $_id = (int) $_POST['id'];
        $_author = htmlspecialchars(strip_tags($_POST['author']));
        $_comment = htmlspecialchars(strip_tags($_POST['message']));

        $_post = $this->Model->create_comment($_id, $_author, $_comment);
        if ($_post) header("Location: /blog/" . $_id . "/");
        else die("MySQL Error: (" . $this->Model->Core->MySQLi->MySQLi->errno . ") " . $this->Model->Core->MySQLi->MySQLi->error);
    }

    # Post page method
    public function method_post()
    {
        $this->Model = new Model_Main();

        $template = $this->Core->Smarty->createTemplate('post.tpl');
        $this->_assign($template);
        $template->assign('post', $this->Model->get_post((int) $_GET['post']));
        $template->assign('comments', $this->Model->get_comments((int) $_GET['post']));

        $template->display();
    }

    # Assign styles and scripts
    private function _assign(&$template)
    {
        $template->assign('title', "MicroBlog");

        $template->assign('styles', array(
            array ('id' => "bootstrap", 'path' => '/js/libs/bootstrap/3.3.7/css/bootstrap.min.css', 'version' => '3.3.7', 'media' => 'all'),
            array ('id' => "bootstrap-theme", 'path' => '/js/libs/bootstrap/3.3.7/css/bootstrap-theme.min.css', 'version' => '3.3.7', 'media' => 'all'),
            array ('id' => "font-awesome", 'path' => '/js/libs/fa/4.7.0/css/font-awesome.min.css', 'version' => '4.7.0', 'media' => 'all'),
            array ('id' => "slick", 'path' => '/js/libs/slick/1.8.0/css/slick.min.css', 'version' => '1.8.0', 'media' => 'all'),
            array ('id' => "slick-theme", 'path' => '/js/libs/slick/1.8.0/css/slick-theme.min.css', 'version' => '1.8.0', 'media' => 'all'),
            array ('id' => "theme", 'path' => '/css/theme.min.css', 'version' => '1.0.0.0', 'media' => 'all')
        ));
        $template->assign('scripts', array(
            array ('path' => '/js/libs/jquery/jquery-3.2.1.min.js', 'version' => '3.2.1'),
            array ('path' => '/js/libs/bootstrap/3.3.7/js/bootstrap.min.js', 'version' => '3.3.7'),
            array ('path' => '/js/libs/slick/1.8.0/js/slick.min.js', 'version' => '1.8.0'),
            array ('path' => '/js/common.min.js', 'version' => '1.0.0')
        ));
    }
}