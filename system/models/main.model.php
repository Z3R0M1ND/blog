<?php
class Model_Main
{
    public $Core;

    function __construct()
    {
        $this->Core = Core::getInstance();
    }

    /**
     * @param $_id
     * @return array
     *
     * return post array fields
     */
    function get_post($_id)
    {
        $_query_post = $this->Core->MySQLi->query("SELECT id,author,content,posted,(SELECT COUNT(*) FROM comments WHERE (post_id = posts.id)) AS comments FROM `posts` WHERE `id`=" . $_id);
        if ($_query_post && $_query_post->num_rows == 1) return $_query_post->fetch_assoc();
        else
        {
            header("Location: /");
            exit;
        }
    }

    /**
     * @param $_id
     * @return array
     *
     * return comments fields
     */
    function get_comments($_id)
    {
        $_comments = [];

        $_query_comments = $this->Core->MySQLi->query("SELECT id,author,comment,posted FROM `comments` WHERE `post_id`=" . $_id . " ORDER BY `posted` DESC");
        if ($_query_comments && $_query_comments->num_rows > 0) while($comment = $_query_comments->fetch_assoc()) $_comments[] = $comment;

        return $_comments;
    }

    /**
     * @param $_author
     * @param $_content
     * @return bool|mixed
     *
     * Create post
     */
    public function create_post($_author, $_content)
    {
        $_insert_post = $this->Core->MySQLi->insert('posts', array('author' => $_author, 'content' => $_content));
        if ($_insert_post) return $this->Core->MySQLi->MySQLi->insert_id;

        return false;
    }

    /**
     * @param $_id
     * @param $_author
     * @param $_comment
     * @return bool|mixed
     *
     * Create comment
     */
    public function create_comment($_id, $_author, $_comment)
    {
        $_insert_comment = $this->Core->MySQLi->insert('comments', array('post_id' => $_id, 'author' => $_author, 'comment' => $_comment));
        if ($_insert_comment) return $this->Core->MySQLi->MySQLi->insert_id;

        return false;
    }

    /**
     * @param array $_args
     * @return array
     *
     * return posts
     */
    public function get_posts($_args = [])
    {
        $_defaults = array (
            'limit'     => 5,
            'order'     => "DESC",
            'orderby'   => "date"
        );

        $args = $this->parse_args($_args, $_defaults);
        $_posts = [];

        switch ($args['orderby'])
        {
            case 'popular':
                $args['orderby'] = "comments";
                $_posts_query = $this->Core->MySQLi->query("SELECT id,author,content,posted,(SELECT COUNT(*) FROM comments WHERE (post_id = posts.id)) AS comments FROM `posts` having comments > 0 ORDER BY `" . $args['orderby'] . "` " . $args['order'] . " LIMIT 0," . $args['limit']);

                break;
            default:
                $args['orderby'] = "posted";
                $_posts_query = $this->Core->MySQLi->query("SELECT id,author,content,posted,(SELECT COUNT(*) FROM comments WHERE (post_id = posts.id)) AS comments FROM `posts` ORDER BY `" . $args['orderby'] . "` " . $args['order'] . " LIMIT 0," . $args['limit']);
        }

        if ($_posts_query && $_posts_query->num_rows > 0) while ($post = $_posts_query->fetch_assoc()) $_posts[] = $post;

        return $_posts;
    }

    /**
     * @param $_args
     * @param $_defaults
     * @return array
     *
     * Merge args
     */
    public function parse_args($_args, $_defaults)
    {
        if (is_object($_args)) $args = get_object_vars($_args);
        elseif (is_array($_args)) $args =& $_args;
        else $args = [];

        if (is_array($_defaults)) return array_merge($_defaults, $args);
        return $args;
    }
}