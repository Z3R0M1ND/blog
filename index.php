<?php
# ERROR REPORTING
error_reporting(E_ALL);

# STATIC DEFINES
define('ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);
define('SYSTEM', ROOT . 'system' . DIRECTORY_SEPARATOR);
define('CORE', SYSTEM . 'core' . DIRECTORY_SEPARATOR);
define('CONFIGS', SYSTEM . 'configs' . DIRECTORY_SEPARATOR);
define('MODELS', SYSTEM . 'models' . DIRECTORY_SEPARATOR);
define('CONTROLLERS', SYSTEM . 'controllers' . DIRECTORY_SEPARATOR);
define('LIBS', SYSTEM . 'libs' . DIRECTORY_SEPARATOR);

# TEMPLATES
define('CACHE', ROOT . 'cache' . DIRECTORY_SEPARATOR);
define('TEMPLATES', ROOT . 'templates' . DIRECTORY_SEPARATOR);
define('TEMPLATES_COMPILE', ROOT . 'templates_c' . DIRECTORY_SEPARATOR);

# INIT CORE
require SYSTEM . 'system.init.php';